import { Controller, Get, Logger, Res } from "@nestjs/common";
import { ApiOperation, ApiResponse } from "@nestjs/swagger";
import { Response } from "express";
import { register } from "prom-client";

@Controller("metrics")
export class MetricsController {
  private readonly logger = new Logger(MetricsController.name);
  @Get()
  @ApiOperation({ summary: "Get metrics" })
  @ApiResponse({ status: 200, description: "Success" })
  async getMetrics(@Res() res: Response) {
    this.logger.log("Inicio flujo Metricas Controller");
    res.set("Content-Type", register.contentType);
    const metrics = await register.metrics();
    res.end(metrics);
  }
}
