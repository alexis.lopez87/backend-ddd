import {
  Controller,
  Post,
  Logger,
  Body,
  Res,
  HttpStatus,
} from "@nestjs/common";
import { ApiOperation, ApiResponse, ApiTags } from "@nestjs/swagger";
import { Response } from "express";
import { PushService } from "../../domain/usescases/push/pushService";
import { PushRequest } from "../push/pushRequest";
import { PushResponse } from "../push/pushResponse";
@ApiTags("Push")
@Controller("Push")
export class AppController {
  private readonly logger = new Logger(AppController.name);

  constructor(private readonly pushService: PushService) {}

  @Post()
  @ApiOperation({ summary: "Push to PubSub GCP" })
  @ApiResponse({ status: 201, description: "Created" })
  @ApiResponse({ status: 500, description: "Not Created" })
  async pushMessage(
    @Body() pushRequest: PushRequest,
    @Res({ passthrough: true }) response: Response
  ) {
    this.logger.log("Inicio flujo publicación PubSub Controller");
    let pushResponse: PushResponse = new PushResponse();

    pushResponse = await this.pushService.pushMessage(pushRequest);
    if (!pushResponse.result) {
      response.status(HttpStatus.INTERNAL_SERVER_ERROR).json(pushResponse);
    } else {
      response.status(HttpStatus.CREATED).json(pushResponse);
    }
  }
}
