export class PushResponse {
  result: boolean;
  messageId: string;
  message: string;
}
