import { PushRequest } from "../../api/push/pushRequest";
import { PushDTO } from "../../domain/models/push/pushDTO";

export class PushUtil {
  pushRequestToPushDTO(pushRequest: PushRequest): PushDTO {
    const pushDTO: PushDTO = new PushDTO();
    pushDTO.name = pushRequest.name;
    pushDTO.email = pushRequest.email;
    return pushDTO;
  }
}
