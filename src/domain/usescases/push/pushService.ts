import { PushResponse } from "../../../api/push/pushResponse";
import { PushConfig } from "../../../infra/config/push.config";
import { PushRequest } from "../../../api/push/pushRequest";
import { PushDTO } from "../../models/push/pushDTO";
import { PushUtil } from "../../../utils/pushUtil/pushUtil";
import { Logger } from "@nestjs/common";

export class PushService {
  private readonly logger = new Logger(PushService.name);
  pushConfig: PushConfig = new PushConfig();

  async pushMessage(pushRequest: PushRequest): Promise<PushResponse> {
    this.logger.log("Inicio flujo publicación PubSub Service");
    const pushResponse = new PushResponse();
    const pushUtil = new PushUtil();
    const pushDTO: PushDTO = pushUtil.pushRequestToPushDTO(pushRequest);

    try {
      const messageId = await this.pushConfig.sendMessage(pushDTO);
      this.logger.log(`Message ${messageId} published.`);
      pushResponse.messageId = messageId;
      pushResponse.message = "Message published";
      pushResponse.result = true;
    } catch (error) {
      this.logger.error(`Received error while publishing: ${error}`);
      pushResponse.message = "Message not published";
      pushResponse.messageId = "0";
      pushResponse.result = false;
    }
    return pushResponse;
  }
}
