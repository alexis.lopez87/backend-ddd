import { NestFactory } from "@nestjs/core";
import { AppModule } from "./infra/server/module";
import { DocumentBuilder, SwaggerModule } from "@nestjs/swagger";
import { MetricsInterceptor } from "./utils/metricsUtil/metricsUtil";
import * as cors from "cors";
import helmet from "helmet";
import * as dotenv from "dotenv";
import { ValidationPipe } from "@nestjs/common";
async function server() {
  const app = await NestFactory.create(AppModule, {
    rawBody: true,
  });

  app.setGlobalPrefix("api");
  app.use(helmet());
  app.use(cors());
  app.useGlobalPipes(new ValidationPipe());
  app.useGlobalInterceptors(new MetricsInterceptor());

  const config = new DocumentBuilder()
    .setTitle("DDD Skeleton")
    .setDescription("Skeleton DDD Typescirpt API")
    .setVersion("1.0")
    .build();

  const document = SwaggerModule.createDocument(app, config);
  SwaggerModule.setup("docs", app, document);

  dotenv.config();
  await app.listen(3000);
}

server();
