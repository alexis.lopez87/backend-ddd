import { Injectable, Logger } from "@nestjs/common";
import { PubSub } from "@google-cloud/pubsub";
import { PushDTO } from "../../domain/models/push/pushDTO";

@Injectable()
export class PushConfig {
  private readonly logger = new Logger(PushConfig.name);
  async sendMessage(pushDTO: PushDTO): Promise<string> {
    this.logger.log("Inicio flujo publicación PubSub PushConfig");
    const pubSubClient = new PubSub();
    const pubSubTopicId = process.env.TOPIC_ID;
    let messageId: string;

    try {
      const message = Buffer.from(JSON.stringify(pushDTO));
      messageId = await pubSubClient.topic(pubSubTopicId).publishMessage({
        data: message,
        attributes: {
          kind: "registration",
        },
      });
      this.logger.log(`Message ${messageId} published.`);
    } catch (error) {
      this.logger.error(`Received error while publishing: ${error.message}`);
      throw new Error(error);
    }
    return messageId;
  }
}
