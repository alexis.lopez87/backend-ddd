import { Module } from "@nestjs/common";
import { AppController } from "../../api/push/pushController";
import { PushService } from "../../domain/usescases/push/pushService";
import { MetricsController } from "../../api/metrics/metricsController";

@Module({
  imports: [],
  controllers: [AppController, MetricsController],
  providers: [PushService],
})
export class AppModule {}
