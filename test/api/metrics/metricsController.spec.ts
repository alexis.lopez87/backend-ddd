import { Test, TestingModule } from "@nestjs/testing";
import { MetricsController } from "../../../src/api/metrics/metricsController";
import { Response } from "express";

describe("AppController", () => {
  let metricController: MetricsController;
  let res: Response;
  beforeEach(async () => {
    const app: TestingModule = await Test.createTestingModule({
      controllers: [MetricsController],
      providers: [],
    }).compile();

    metricController = app.get<MetricsController>(MetricsController);
  });

  describe("root", () => {
    it('should return "Hello, this is your example data!"', () => {
      return expect(metricController.getMetrics(res)).toBeCalled;
    });
  });
});
