import { Test, TestingModule } from "@nestjs/testing";
import { AppController } from "../../../src/api/push/pushController";
import { PushRequest } from "src/api/push/pushRequest";
import { Response } from "express";
describe("AppController", () => {
  let appController: AppController;

  beforeEach(async () => {
    const app: TestingModule = await Test.createTestingModule({
      controllers: [AppController],
      providers: [],
    }).compile();

    appController = app.get<AppController>(AppController);
  });

  describe("root", () => {
    const pushRequest = new PushRequest();
    let resp: Response;

    pushRequest.name = "Juan";
    pushRequest.email = "aaa@aa.cl";

    it('should return "Hello, this is your example data!"', () => {
      return expect(appController.pushMessage(pushRequest, resp)).toBeCalled();
    });
  });
});
