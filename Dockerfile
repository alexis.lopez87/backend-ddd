FROM node:lts-alpine

WORKDIR /usr/src/app

COPY package*.json ./

RUN npm install

COPY . .

RUN npm run format

RUN npm run build

EXPOSE 3000

CMD ["npm", "run", "start:prod"]
#docker build -f Dockerfile -t ejemplo-ddd .
#docker run --rm -it \-p 3000:3000 \--env-file ./.env \ejemplo-ddd